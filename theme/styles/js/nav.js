;(function($) {
    $.fn.ssdVerticalNavigation = function(options) {
        "use strict";
        var settings = $.extend({
            classMaster : 'leftMenu',
            classActive : 'opened',
            classClickable : 'clickable',
            classChevron : 'menuEntryChevron'
        }, options);
        function _leftNavigationActiveMain(thisLi) {
            "use strict";
            thisLi
                .toggleClass(settings.classActive)
                .siblings()
                .removeClass(settings.classActive);
        }
        function _leftNavigationClick(thisParentUl, thisLi, event) {
            "use strict";
            if (thisLi.has("ul").length != 0) {
            	$('.sidebar').removeClass('collapsed');
            	$('.nav-burger-icon').addClass('open');
                event.preventDefault();
                event.stopPropagation();
                _leftNavigationActiveMain(thisLi);
            }
        }
        return this.each(function() {
            "use strict";
            $(this)
                .addClass(settings.classMaster)
                .on('click',  'li a', function(event) {
                try {
                    var thisA = $(this),
                        thisLi = thisA.parent('li'),
                        thisParentUl = thisLi.parent('ul');
                    _leftNavigationClick(thisParentUl, thisLi, event);
                } catch (errorMessage) {
                    console.log(errorMessage);
                }
            })
            .on('click',  'li span', function(event) {
                try {
                    var thisA = $(this),
                        thisLi = thisA.parent('li'),
                        thisParentUl = thisLi.parent('ul');
                    _leftNavigationClick(thisParentUl, thisLi, event);
                    setTimeout(function(){
                    	if(jQuery('.sidebar-header').height()+jQuery('.sidebar-body').height() - 20 >= jQuery(window).height()){
                    		jQuery('.sidebar-scroll-fixed').css('overflow-y', 'scroll');
                    		jQuery('.sidebar-scroll-fixed').css('height', '100%');
                    	}
                    	else{
                    		jQuery('.sidebar-scroll-fixed').css('overflow-y', 'auto');
                    	}
                    }, 500);
                } catch (errorMessage) {
                    console.log(errorMessage);
                }
            });
            $(this).find('li').each(function() {
            	$(this).children('a, span').each(function(){
        			$(this).attr('title', $(this).text());
            	});
            	if($(this).has('ul').length != 0){
            		$(this).addClass('sitemap-folder');
            		$(this).children('a').each(function(){
            			$(this).append('<span class="'+settings.classChevron+'"></span>');
            		})
            		$(this).children('span').each(function(){
            			$(this).append('<span class="'+settings.classChevron+'"></span>');
            		})
            	}
            })
        });
    }
}(jQuery));

$(function() {
    $('.main-sidebar .sidebar .sitemap-menu-root').ssdVerticalNavigation();
    $('.main-sidebar .sidebar .sitemap-menu-root').find('.icon').each(function(){
    	if($(this).css('background-image') != 'none'){
    		$(this).css('padding-left','40px')
    	}
    })
        
    $('.nav-burger-icon, .main-sidebar .sidebar .sidebar-header .searchEntry, .main-sidebar .sidebar .sidebar-header .profile .userPhoto').click(function(event){
    	event.preventDefault();
        event.stopPropagation();
        $('.nav-burger-icon').toggleClass('open');
        $('.sidebar').toggleClass('collapsed');
        $('.main-sidebar .sidebar .sitemap-menu-root .opened').each(function(){
        	$(this).removeClass('opened');
        });
        if($(this).hasClass('searchEntry')){
        	$('.main-sidebar .sidebar .sidebar-header .sidebar-search input[type="search"]').focus();
        }
        $('.dash-margin').css('margin-left', $('.main-sidebar .sidebar').width()+'px')
        return false;
    });
    
    $(document).click(function(event) {
    	if(!$(event.target).is('.main-sidebar *') && !$(event.target).is('input, select, textarea')){
    		if(!$('.main-sidebar .sidebar').hasClass('lock-open')){
    			$('.main-sidebar .sidebar').addClass('collapsed');
            	$('.nav-burger-icon').removeClass('open');
        		$('.dash-margin').css('margin-left', $('.main-sidebar .sidebar').width()+'px')
            	$('.main-sidebar .sidebar .sitemap-menu-root .opened').each(function(){
                	$(this).removeClass('opened');
                });
    		}
    	}
    });
    
    $(document).ready(function() {
    	if(jQuery(window).width() < 1600){
    		$('.main-sidebar .sidebar').addClass('collapsed');
    		$('.nav-burger-icon').removeClass('open');
    	}
    	$('.dash-margin').css('margin-left', $('.main-sidebar .sidebar').width()+'px')
    	if(jQuery('.sidebar-header').height()+jQuery('.sidebar-body').height() - 20 >= jQuery(window).height()){
    		jQuery('.sidebar-scroll-fixed').css('overflow-y', 'scroll');
    		jQuery('.sidebar-scroll-fixed').css('height', '100%');
    	}
    })	
});

jQuery('.sidebar-scroll-fixed').resize(function(){
    if(jQuery('.sidebar-header').height()+jQuery('.sidebar-body').height() - 20 >= jQuery(window).height()){
		jQuery('.sidebar-scroll-fixed').css('overflow-y', 'scroll');
		jQuery('.sidebar-scroll-fixed').css('height', '100%');
	}
})